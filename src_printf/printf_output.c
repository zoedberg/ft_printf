#include "ft_printf.h"

/*==================================================*/
/*					-=OUTPUT=-						*/
/*==================================================*/

void	std_space(int value, t_get_flag *descr)
{
	while (value > 0)
	{
		if (descr->zero == 1)
		{
			if (descr->plus == 1)
			{
				ft_putstr("+");
				descr->plus = 0;
			}
		ft_putstr("0");
		}
		else
			ft_putstr(" ");
		descr->length++;
		value--;
	}
}

int		std_str(char *o_str, t_get_flag *descr)
{
	int i;

	i = 0;
	if (descr->plus == 1 && o_str[0] != '-')
	{
		ft_putstr("+");
		i++;
	}
	if (descr->space == 1 && descr->width == 0 && o_str[0] != '-')
	{
		ft_putstr(" ");
		i++;
	}
	ft_putstr(o_str);
	descr->length += i;
	return (i);
}

void	output_2(t_get_flag *descr, char *o_str)
{
	int i;

	if ((size_t)descr->width > ft_strlen(o_str))
		if (descr->minus == 1)
		{
			i = std_str(o_str, descr);
			std_space(descr->width - ft_strlen(o_str) - i, descr);
		}
		else
		{
			descr->width -= ft_strlen(o_str);
			descr->plus == 1 ? descr->width -= 1 : 0;
			descr->sharp == 1 ? descr->width -= 1 : 0;
			std_space(descr->width, descr);
			std_str(o_str, descr);
		}
	else
		std_str(o_str, descr);
}

void	output_1(t_get_flag *descr, char *o_str)
{
	int i;

	if ((size_t)descr->width > ft_strlen(o_str))
		if (descr->minus == 1)
		{
			descr->space == 1 ? ft_putstr(" ") : 0;
			descr->plus == 1 ? ft_putstr("+") : 0;
			while (--descr->precision > ft_strlen(o_str) - 1)
			{
				ft_putstr("0");
				descr->width--;
			}
			ft_putstr(o_str);
			std_space(descr->width - ft_strlen(o_str) - i, descr);
		}
		else
		{
			descr->space == 1 ? ft_putstr(" ") : 0;
			descr->plus == 1 ? ft_putstr("+") : 0;
			if (descr->precision < descr->width)
				while (descr->precision - 1 < --descr->width)
					ft_putstr(" ");
			while (--descr->precision > ft_strlen(o_str) - 1)
			{
				ft_putstr("0");
				descr->width--;
			}
			ft_putstr(o_str);
		}
	else
	{
		descr->space == 1 ? ft_putstr(" ") : 0;
		descr->plus == 1 ? ft_putstr("+") : 0;
		if (descr->precision != -1)
			while (--descr->precision > ft_strlen(o_str) - 1)
				ft_putstr("0");
		ft_putstr(o_str);
	}
}

void	output_line_setting(t_get_flag *descr, char *o_str)
{
	if (descr->precision == -1)
		output_2(descr, o_str);
	else
		output_1(descr, o_str);
}

void	print_sharp(int value, char *o_str)
{
	if (value == 8 && o_str[0] != '0')
		ft_putchar('0');
	else if (value == 16)
		ft_putstr("0x");
	else if (value == 17)
		ft_putstr("0X");
}

void	sharp_output_setting(t_get_flag *descr, char *o_str, int value)
{
	int i;

	if ((size_t)descr->width > ft_strlen(o_str))
		if (descr->minus == 1)
		{
			descr->sharp == 1 ? print_sharp(value, o_str) : 0;
			i = std_str(o_str, descr);
			std_space(descr->width - ft_strlen(o_str) - i, descr);
		}
		else
		{
			descr->width -= ft_strlen(o_str);
			descr->plus == 1 ? descr->width -= 1 : 0;
			(descr->sharp == 1 && (value == 16 || value == 17)) ? descr->width -= 2 : 0;
			(descr->sharp == 1 && value == 8) ? descr->width -= 1 : 0;
			std_space(descr->width, descr);
			descr->sharp == 1 ? print_sharp(value, o_str) : 0;
			std_str(o_str, descr);
		}
	else
	{
		(descr->sharp == 1) ? print_sharp(value, o_str) : 0;
		std_str(o_str, descr);
	}
}
