#include "ft_printf.h"

/*==================================================*/
/*					-=3_PART_PRECISION=-			*/
/*==================================================*/

int		find_precision(char *str, int *start, t_get_flag *descr)
{
	int temp;

	temp = *start;
	if (str[*start] == '.')
	{
		temp++;
		while (str[temp] >= '0' && str[temp] <= '9')
			temp++;
		descr->precision = ft_atoi(ft_strsub(str, *start + 1, temp));
		*start += temp - *start;
		return (1);
	}
	return (0);
}

int		find_flag_precision(char *str, int *start, t_get_flag *descr)
{
	while (str[*start])
	{
		if (find_precision(str, start, descr) == 1)
			return (1);
		if ((str[*start] >= 'a' && str[*start] <= 'z')
			|| (str[*start] >= 'A' && str[*start] <= 'Z'))
			return (1);
		else
			return (-1);
	}
	return (0);
}

/*==================================================*/
/*					-=2_PART_WIDTH=-				*/
/*==================================================*/

int		find_width(char *str, int *start, t_get_flag *descr)
{
	int temp;

	temp = *start;
	if (str[*start] >= '1' && str[*start] <= '9')
	{
		while(str[temp] >= '0' && str[temp] <= '9')
			temp++;
		descr->width = ft_atoi(ft_strsub(str, *start, temp));
		*start += temp - *start;
		return (1);
	}
	return (0);
}

int		find_flag_width(char *str, int *start, t_get_flag *descr)
{
	while (str[*start])
	{
		if (find_width(str, start, descr) == 1)
			return (1);
		if ((str[*start] >= 'a' && str[*start] <= 'z')
			|| (str[*start] >= 'A' && str[*start] <= 'Z'))
			return (1);
		else
			return (0);
	}
	return (0);
}

/*==================================================*/
/*					-=1_PART "#0+- " =-				*/
/*==================================================*/

int		find_symb(char symb, t_get_flag *descr)
{
	if (symb == '0')
	{
		descr->zero = 1;
		return (1);
	}
	if (symb == '-' )
	{
		descr->minus = 1;
		return (1);
	}
	if (symb == '+')
	{
		descr->plus = 1;
		return (1);
	}
	if (symb == '#' )
	{
		descr->sharp = 1;
		return (1);
	}
	if (symb == ' ' )
	{
		descr->space = 1;
		return (1);
	}
	return (0);
}

int		find_flag_symbol(char *str, int *start, t_get_flag *descr)
{
	while (str[*start])
	{
		if ((find_symb(str[*start], descr) == 0)
			&& !((str[*start] >= 'a' && str[*start] <= 'z')
			|| (str[*start] >= 'A' && str[*start] <= 'Z')))
				return (0);
		*start += 1;
		if ((str[*start] >= 'a' && str[*start] <= 'z')
			|| (str[*start] >= 'A' && str[*start] <= 'Z'))
			return (1);
	}
	return (0);
}
