#include "ft_printf.h"

/*==================================================*/
/*		-= 5_PART "l, hh, h, ll, j, z, t, L" =-		*/
/*==================================================*/

int		check_size_specifier(char *str, int *start)
{
	if (str[*start] == 'l' && str[*start + 1] == 'l')
	{
		*start += 2;
		return (1);
	}
	else if (str[*start] == 'l')
	{
		*start += 1;
		return (2);
	}
	else if (str[*start] == 'h' && str[*start + 1] == 'h')
	{
		*start += 2;
		return (3);
	}
	else if (str[*start] == 'h')
	{
		*start += 1;
		return (4);
	}
	else if (str[*start] == 'j')
	{
		*start += 1;
		return (5);
	}
	else if (str[*start] == 'z')
	{
		*start += 1;
		return (6);
	}
	else if (str[*start] == 't')
	{
		*start += 1;
		return (7);
	}
	else if (str[*start] == 'L')
	{
		*start += 1;
		return (8);
	}
	return (0);
}

/*==================================================*/
/*			-= 4_PART "f d i c zu..." =-			*/
/*==================================================*/

void	func_int(t_get_flag *descr, int type)
{
	if (type == 0)
		output_line_setting(descr, ft_itoa(va_arg(*descr->arg, int)));
	else if (type == 1)
		output_line_setting(descr, ft_itoa(va_arg(*descr->arg, long long int)));
	else if (type == 2)
		output_line_setting(descr, ft_itoa(va_arg(*descr->arg, long int)));
	else if (type == 3)
		output_line_setting(descr, ft_itoa((char)va_arg(*descr->arg, int)));
	else if (type == 4)
		output_line_setting(descr, ft_itoa((short int)va_arg(*descr->arg, int)));
	else if (type == 5)
		output_line_setting(descr, ft_itoa(va_arg(*descr->arg, intmax_t)));
	else if (type == 6)
		output_line_setting(descr, ft_itoa(va_arg(*descr->arg, size_t)));
	else if (type == 7)
		output_line_setting(descr, ft_itoa(va_arg(*descr->arg, ptrdiff_t)));
	else if (type == 8)
		output_line_setting(descr, ft_itoa(va_arg(*descr->arg, size_t)));
}

void	func_float_small(t_get_flag *descr)
{
	int precision;

	descr->precision == -1 ? (precision = 6) : (precision = descr->precision);
	output_line_setting(descr, ftoa_d((float)va_arg(*descr->arg, double), precision));
}

void	func_float_big(t_get_flag *descr, int type)
{
	int precision;

	descr->precision == -1 ? (precision = 6) : (precision = descr->precision);
	if (type == 8)
		output_line_setting(descr, ftoa_d(va_arg(*descr->arg, long double), precision));
	else
		output_line_setting(descr, ftoa_d(va_arg(*descr->arg, double), precision));
}

void	func_long_decimal(t_get_flag *descr, int type)
{
	output_line_setting(descr, ft_itoa(va_arg(*descr->arg, intmax_t)));
}

void	func_unsigned(t_get_flag *descr, int type)
{
	if (type == 0)
		output_line_setting(descr, ft_itoa_u(va_arg(*descr->arg, unsigned int)));
	else if (type == 1)
		output_line_setting(descr, ft_itoa_u(va_arg(*descr->arg, unsigned long long int)));
	else if (type == 2)
		output_line_setting(descr, ft_itoa_u(va_arg(*descr->arg, unsigned long long int)));
	else if (type == 3)
		output_line_setting(descr, ft_itoa_u((unsigned char)va_arg(*descr->arg, unsigned int)));
	else if (type == 4)
		output_line_setting(descr, ft_itoa_u((unsigned short int)va_arg(*descr->arg, unsigned int)));
	else if (type == 5)
		output_line_setting(descr, ft_itoa_u(va_arg(*descr->arg, intmax_t)));
	else if (type == 6)
		output_line_setting(descr, ft_itoa_u(va_arg(*descr->arg, size_t)));
	else if (type == 7)
		output_line_setting(descr, ft_itoa_u(va_arg(*descr->arg, ptrdiff_t)));
	else if (type == 8)
		output_line_setting(descr, ft_itoa_u(va_arg(*descr->arg, size_t)));
}

void	func_string(t_get_flag *descr)
{
	int		i;
	char	*str;

	str = va_arg(*descr->arg, char *);
	i = -1;
	if ((size_t)descr->width > ft_strlen(str))
	{
		if (descr->minus == 1)
		{
			ft_putstr(str);
			while (++i < descr->width - ft_strlen(str))
				ft_putchar(' ');
		}
		else
		{
			while (++i < descr->width - ft_strlen(str))
				ft_putchar(' ');
			ft_putstr(str);
		}
	}
	else
		ft_putstr(str);
}

void	func_wstring(t_get_flag *descr)
{
	int		i;
	wchar_t	*str;

	str = va_arg(*descr->arg, wchar_t *);
	i = -1;
	if ((size_t)descr->width > ft_strlen_w(str))
	{
		if (descr->minus == 1)
		{
			ft_putwstr(str);
			while (++i < descr->width - ft_strlen_w(str))
				ft_putchar(' ');
		}
		else
		{
			while (++i < descr->width - ft_strlen_w(str))
				ft_putchar(' ');
			ft_putwstr(str);
		}
	}
	else
		ft_putwstr(str);
}

void	func_hex_small(t_get_flag *descr, int type)
{
	if (type == 0)
		sharp_output_setting(descr, ft_itoa_base(va_arg(*descr->arg, int), 16, 0), 16);
	else if (type == 1)
		sharp_output_setting(descr, ft_itoa_base(va_arg(*descr->arg, long long int), 16, 0), 16);
	else if (type == 2)
		sharp_output_setting(descr, ft_itoa_base(va_arg(*descr->arg, long int), 16, 0), 16);
	else if (type == 3)
		sharp_output_setting(descr, ft_itoa_base((char)va_arg(*descr->arg, int), 16, 0), 16);
	else if (type == 4)
		sharp_output_setting(descr, ft_itoa_base((short int)va_arg(*descr->arg, int), 16, 0), 16);
	else if (type == 5)
		sharp_output_setting(descr, ft_itoa_base(va_arg(*descr->arg, intmax_t), 16, 0), 16);
	else if (type == 6)
		sharp_output_setting(descr, ft_itoa_base(va_arg(*descr->arg, size_t), 16, 0), 16);
	else if (type == 7)
		sharp_output_setting(descr, ft_itoa_base(va_arg(*descr->arg, ptrdiff_t), 16, 0), 16);
	else if (type == 8)
		sharp_output_setting(descr, ft_itoa_base(va_arg(*descr->arg, size_t), 16, 0), 16);
}

void	func_hex_big(t_get_flag *descr, int type)
{
	if (type == 0)
		sharp_output_setting(descr, ft_itoa_base(va_arg(*descr->arg, int), 16, 1), 17);
	else if (type == 1)
		sharp_output_setting(descr, ft_itoa_base(va_arg(*descr->arg, long long int), 16, 1), 17);
	else if (type == 2)
		sharp_output_setting(descr, ft_itoa_base(va_arg(*descr->arg, long int), 16, 1), 17);
	else if (type == 3)
		sharp_output_setting(descr, ft_itoa_base((char)va_arg(*descr->arg, int), 16, 1), 17);
	else if (type == 4)
		sharp_output_setting(descr, ft_itoa_base((short int)va_arg(*descr->arg, int), 16, 1), 17);
	else if (type == 5)
		sharp_output_setting(descr, ft_itoa_base(va_arg(*descr->arg, intmax_t), 16, 1), 17);
	else if (type == 6)
		sharp_output_setting(descr, ft_itoa_base(va_arg(*descr->arg, size_t), 16, 1), 17);
	else if (type == 7)
		sharp_output_setting(descr, ft_itoa_base(va_arg(*descr->arg, ptrdiff_t), 16, 1), 17);
	else if (type == 8)
		sharp_output_setting(descr, ft_itoa_base(va_arg(*descr->arg, size_t), 16, 1), 17);

}

void	func_octa(t_get_flag *descr, int type)
{
	if (type == 0)
		sharp_output_setting(descr, ft_itoa_base(va_arg(*descr->arg, int), 8, 0), 8);
	else if (type == 1)
		sharp_output_setting(descr, ft_itoa_base(va_arg(*descr->arg, long long int), 8, 0), 8);
	else if (type == 2)
		sharp_output_setting(descr, ft_itoa_base(va_arg(*descr->arg, long int), 8, 0), 8);
	else if (type == 3)
		sharp_output_setting(descr, ft_itoa_base((char)va_arg(*descr->arg, int), 8, 0), 8);
	else if (type == 4)
		sharp_output_setting(descr, ft_itoa_base((short int)va_arg(*descr->arg, int), 8, 0), 8);
	else if (type == 5)
		sharp_output_setting(descr, ft_itoa_base(va_arg(*descr->arg, intmax_t), 8, 0), 8);
	else if (type == 6)
		sharp_output_setting(descr, ft_itoa_base(va_arg(*descr->arg, size_t), 8, 0), 8);
	else if (type == 7)
		sharp_output_setting(descr, ft_itoa_base(va_arg(*descr->arg, ptrdiff_t), 8, 0), 8);
	else if (type == 8)
		sharp_output_setting(descr, ft_itoa_base(va_arg(*descr->arg, size_t), 8, 0), 8);
}

void	func_binary(t_get_flag *descr, int type)
{
	if (type == 0)
		output_line_setting(descr, ft_itoa_base(va_arg(*descr->arg, int), 2, 0));
	else if (type == 1)
		output_line_setting(descr, ft_itoa_base(va_arg(*descr->arg, long long int), 2, 0));
	else if (type == 2)
		output_line_setting(descr, ft_itoa_base(va_arg(*descr->arg, long int), 2, 0));
	else if (type == 3)
		output_line_setting(descr, ft_itoa_base((char)va_arg(*descr->arg, int), 2, 0));
	else if (type == 4)
		output_line_setting(descr, ft_itoa_base((short int)va_arg(*descr->arg, int), 2, 0));
	else if (type == 5)
		output_line_setting(descr, ft_itoa_base(va_arg(*descr->arg, intmax_t), 2, 0));
	else if (type == 6)
		output_line_setting(descr, ft_itoa_base(va_arg(*descr->arg, size_t), 2, 0));
	else if (type == 7)
		output_line_setting(descr, ft_itoa_base(va_arg(*descr->arg, ptrdiff_t), 2, 0));
	else if (type == 8)
		output_line_setting(descr, ft_itoa_base(va_arg(*descr->arg, size_t), 2, 0));
}

void	func_char(t_get_flag *descr)
{
	int i;
	char	symb = (char)va_arg(*descr->arg, int);

	i = -1;
	if ((size_t)descr->width > 1)
	{
		if (descr->minus == 1)
		{
			ft_putchar(symb);
			while (++i < descr->width - 1)
				ft_putchar(' ');
		}
		else
		{
			while (++i < descr->width - 1)
				ft_putchar(' ');
			ft_putchar(symb);
		}
	}
	else
		ft_putchar(symb);
}

void	func_wchar(t_get_flag *descr)
{
	int		i;
	wchar_t	symb = (wchar_t)va_arg(*descr->arg, int);

	i = -1;
	if ((size_t)descr->width > 1)
	{
		if (descr->minus == 1)
		{
			ft_putwchar(symb);
			while (++i < descr->width - 1)
				ft_putchar(' ');
		}
		else
		{
			while (++i < descr->width - 1)
				ft_putchar(' ');
			ft_putwchar(symb);
		}
	}
	else
		ft_putwchar(symb);
}

void	func_pointer(t_get_flag *descr)
{
	size_t	num;
	char	*s;
	int		i;

	num = (size_t)va_arg(*descr->arg, void *);
	s = (char *)malloc(sizeof(num) * 2);
	i = 2 * sizeof(num);
	while (--i >= 0)
	{
		s[i] = "0123456789abcdef"[num & 0x0F];
	    num >>= 4;
	}
	ft_putstr("0x");
	i = 0;
	while (s[i] == '0')
		i++;
	(int)i >= (int)(2 * sizeof(num)) ? ft_putchar('0') : 0;
	while (i < (int)(2 * sizeof(num)))
	{
		ft_putchar(s[i]);
		++i;
	}
}

int		find_flag_character(char *str, int *start, t_get_flag *descr)
{
	int type;

	check_flag_compatability(descr);
	type = check_size_specifier(str, start);
	str[*start] == 's' ? func_string(descr) : 0;
	str[*start] == 'S' ? func_wstring(descr) : 0;
	str[*start] == 'p' ? func_pointer(descr) : 0;
	str[*start] == 'd' ? func_int(descr, type) : 0;
	str[*start] == 'D' ? func_long_decimal(descr, type) : 0;
	str[*start] == 'i' ? func_int(descr, type) : 0;
	str[*start] == 'f' ? func_float_small(descr) : 0;
	str[*start] == 'F' ? func_float_big(descr, type) : 0;
	str[*start] == 'u' ? func_unsigned(descr, type) : 0;
	str[*start] == 'o' ? func_octa(descr, type) : 0;
	str[*start] == 'c' ? func_char(descr) : 0;
	str[*start] == 'C' ? func_wchar(descr) : 0;
	str[*start] == 'b' ? func_binary(descr, type) : 0;
	str[*start] == 'x' ? func_hex_small(descr, type) : 0;
	str[*start] == 'X' ? func_hex_big(descr, type) : 0;
	return (-1);
}

/*==================================================*/
/*					-=VALIDASHKA=-					*/
/*==================================================*/

int		validation(char *str, int *start, t_get_flag *descr)
{
	int temp;

	if ((str[*start] >= 'a' && str[*start] <= 'z')
		|| (str[*start] >= 'A' && str[*start] <= 'Z'))
	{
		find_flag_character(str, start, descr);
	}
	else
	{
		if (find_flag_symbol(str, start, descr) == 1)
			validation(str, start, descr);
		else if (find_flag_width(str, start, descr) == 1)
			validation(str, start, descr);
		else if ((temp = find_flag_precision(str, start, descr)) == 1)
			validation(str, start, descr);
		else if (temp == -1)
			return (-1);
		else
			find_flag_character(str, start, descr);
	}
	return (0);
}

/*==================================================*/
/*					-=PRINTF=-						*/
/*==================================================*/

size_t	ft_printf(char *format, ...)
{
	t_get_flag	descr;
	va_list		arg;
	int			i;

	i = -1;
	va_start(arg, format);
	descr.arg = &arg;
	while (format[++i])
		if (format[i] == '%')
		{
			i++;
			if (format[i] != '%')
			{
				full_destruction(&descr);
				if (validation(format, &i, &descr) == -1)
					return (-1);
				// show_struct(&descr);
			}
			else
				ft_putstr("%");
		}
		else
			ft_putchar(format[i]);
	va_end(arg);
	return (descr.length);
}
