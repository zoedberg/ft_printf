# ifndef FT_PRINTF_H
# define FT_PRINTF_H

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <inttypes.h>
#include <stddef.h>

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[91m"
#define KGRN  "\x1B[92m"
#define KYEL  "\x1B[93m"
#define KBLU  "\x1B[94m"
#define KMAG  "\x1B[95m"
#define KCYN  "\x1B[96m"
#define KWHT  "\x1B[97m"

/*			-=DESCRIBING_FLAGS_STRUCTURE	*/

typedef struct		s_get_flag
{
	char	zero;
	char	sharp;
	char	minus;
	char	plus;
	char	space;
	int		precision;
	int		width;
	size_t	length;
	va_list	*arg;
}					t_get_flag;

/*				-=MAIN_FUNCTIONS=-			*/
size_t				ft_printf(char *format, ...);

/*				-=PRINTF_TOOLS=-			*/
void				check_flag_compatability(t_get_flag *descr);
void				full_destruction(t_get_flag *p);

/*			-=FIND_FLAGS "#-+0 etc."=-		*/
int					find_flag_precision(char *str, int *start, t_get_flag *descr);
int					find_flag_character(char *str, int *start, t_get_flag *descr);
int					find_flag_symbol(char *str, int *start, t_get_flag *descr);
int					find_flag_width(char *str, int *start, t_get_flag *descr);
int					validation(char *str, int *start, t_get_flag *descr);

/*					-=OUTPUT=-				*/
void				sharp_output_setting(t_get_flag *descr, char *o_str, int value);
void				output_line_setting(t_get_flag *descr, char *o_str);
void				std_space(int value, t_get_flag *descr);
int					std_str(char *o_str, t_get_flag *descr);

/*				-=SHOW_DETAILS=-			*/
void				show_struct(t_get_flag *p);

/*					-=TOOLS=-				*/
char				*ft_strsub(char const *s, unsigned int start, size_t len);
char				*ft_itoa_base(long long int value, int base, int size);
char				*ftoa_d(long double num, int precision);
char				*ft_itoa_u(unsigned long long int n);
char				*ftoa_f(float num, int precision);
size_t				ft_strlen_w(const wchar_t *s);
int					ft_atoi(const char *string);
void				ft_putwstr(wchar_t *str);
size_t				ft_strlen(const char *s);
void				ft_putstr(char *str);
char				*ft_itoa(intmax_t n);
void				ft_putwchar(char w);
void				ft_putchar(char c);
#endif
