#include "ft_printf.h"

/*==================================================*/
/*					-=RESET_OPTIONS=-				*/
/*==================================================*/

void	full_destruction(t_get_flag *p)
{
	p->zero = 0;
	p->sharp = 0;
	p->minus = 0;
	p->plus = 0;
	p->space = 0;
	p->precision = -1;
	p->width = 0;
}

/*==================================================*/
/*			-=EXCEPTIONS_AND_FLAG COMPATABILITY=-	*/
/*==================================================*/

void check_flag_compatability(t_get_flag *descr)
{
	if (descr->minus == 1 && descr->zero == 1)
		descr->zero = 0;
	if (descr->plus == 1 && descr->space == 1)
		descr->space = 0;
}
