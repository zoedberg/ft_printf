#include "../ft_printf.h"

void	show_struct(t_get_flag *p)
{

	if (p->zero != 0)
		printf(KGRN "\n[zero]      : %i%s\n", (int)p->zero, KNRM);
	if (p->sharp != 0)
		printf(KGRN "\n[sharp]     : %i%s\n", (int)p->sharp, KNRM);
	if (p->minus != 0)
		printf(KGRN "\n[minus]     : %i%s\n", (int)p->minus, KNRM);
	if (p->plus != 0)
		printf(KGRN "\n[plus]      : %i%s\n", (int)p->plus, KNRM);
	if (p->space != 0)
		printf(KGRN "\n[space]     : %i%s\n", (int)p->space, KNRM);
	if (p->width != 0)
		printf(KGRN "\n[width]     : %i%s\n", (int)p->width , KNRM);
	if (p->precision != -1)
		printf(KGRN "\n[precision] : %i%s\n", (int)p->precision, KNRM);
}
