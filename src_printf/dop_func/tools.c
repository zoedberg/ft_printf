#include "../ft_printf.h"
#include <wchar.h>

#define A(x) ((x) < 0 ? -(x) : (x))

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putwchar(char w)
{
    if(w<128) putchar(w);
    else if(w<2048) { putchar(w>>6 | 0xC0); putchar(w&63 | 0x80); }
    else if(w<1<<16) { putchar(w>>12 | 0xE0); putchar(w>>6 & 63 | 0x80); putchar(w&63 | 0x80);}
    else if(w<1<<21) { putchar(w>>18 | 0xF0); putchar(w>>12 & 63 | 0x80); putchar(w>>6 & 63 | 0x80); putchar(w&63 | 0x80); }
    else if(w<1<<26){ putchar(w>>24 | 0xF8); putchar(w>>18 & 63 | 0x80); putchar(w>>12 & 63 | 0x80); putchar(w>>6 & 63 | 0x80); putchar(w&63 | 0x80); }
    else{ putchar(w>>30 | 0xFC); putchar(w>>24 & 63 | 0x80); putchar(w>>18 & 63 | 0x80); putchar(w>>12 & 63 | 0x80); putchar(w>>6 & 63 | 0x80); putchar(w&63 | 0x80); }
}

size_t		ft_strlen(const char *s)
{
	int		length;

	length = 0;
	while (s[length])
		length++;
	return (length);
}

size_t		ft_strlen_w(const wchar_t *s)
{
	int		length;

	length = 0;
	while (s[length])
		length++;
	return (length);
}

void ft_putwstr(wchar_t *str)
{
	int i;

	i = -1;
	while (str[++i] != '\0')
		ft_putwchar(str[i]);
		// if((size_t)str[i]<128)
		// 	ft_putchar(str[i]);
		// else if((size_t)str[i]<2048)
		// {
		// 	ft_putchar(str[i]>>6 | 0xC0);
		// 	ft_putchar(str[i] & (63 | 0x80));
		// }
		// else if((size_t)str[i]<1<<16)
		// {
		// 	ft_putchar(str[i]>>12 | 0xE0);
		// 	ft_putchar(str[i]>>6 & (63 | 0x80));
		// 	ft_putchar(str[i]& (63 | 0x80));
		// }
		// else if((size_t)str[i]<1<<21)
		// {
		// 	ft_putchar(str[i]>>18 | 0xF0);
		// 	ft_putchar(str[i]>>12 & (63 | 0x80));
		// 	ft_putchar(str[i]>>6 & (63 | 0x80));
		// 	ft_putchar(str[i] & (63 | 0x80));
		// }
		// else if((size_t)str[i]<1<<26)
		// {
		// 	ft_putchar(str[i]>>24 | 0xF8);
		// 	ft_putchar(str[i]>>18 & (63 | 0x80));
		// 	ft_putchar(str[i]>>12 & (63 | 0x80));
		// 	ft_putchar(str[i]>>6 & (63 | 0x80));
		// 	ft_putchar(str[i]& (63 | 0x80));
		// }
		// else
		// {
		// 	ft_putchar(str[i]>>30 | 0xFC);
		// 	ft_putchar(str[i]>>24 & (63 | 0x80));
		// 	ft_putchar(str[i]>>18 & (63 | 0x80));
		// 	ft_putchar(str[i]>>12 & (63 | 0x80));
		// 	ft_putchar(str[i]>>6 & (63 | 0x80));
		// 	ft_putchar(str[i] & (63 | 0x80));
		// }
}

void ft_putstr(char *str)
{
	int i;

	i = -1;
	while (str[++i] != '\0')
		if((size_t)str[i]<128)
		{
			ft_putchar(str[i]);
		}
		else if((size_t)str[i]<2048)
		{
			ft_putchar(str[i]>>6 | 0xC0);
			ft_putchar(str[i] & (63 | 0x80));
		}
		else if((size_t)str[i]<1<<16)
		{
			ft_putchar(str[i]>>12 | 0xE0);
			ft_putchar(str[i]>>6 & (63 | 0x80));
			ft_putchar(str[i]& (63 | 0x80));
		}
		else if((size_t)str[i]<1<<21)
		{
			ft_putchar(str[i]>>18 | 0xF0);
			ft_putchar(str[i]>>12 & (63 | 0x80));
			ft_putchar(str[i]>>6 & (63 | 0x80));
			ft_putchar(str[i] & (63 | 0x80));
		}
		else if((size_t)str[i]<1<<26)
		{
			ft_putchar(str[i]>>24 | 0xF8);
			ft_putchar(str[i]>>18 & (63 | 0x80));
			ft_putchar(str[i]>>12 & (63 | 0x80));
			ft_putchar(str[i]>>6 & (63 | 0x80));
			ft_putchar(str[i]& (63 | 0x80));
		}
		else
		{
			ft_putchar(str[i]>>30 | 0xFC);
			ft_putchar(str[i]>>24 & (63 | 0x80));
			ft_putchar(str[i]>>18 & (63 | 0x80));
			ft_putchar(str[i]>>12 & (63 | 0x80));
			ft_putchar(str[i]>>6 & (63 | 0x80));
			ft_putchar(str[i] & (63 | 0x80));
		}
}

char	*ft_strnew(size_t size)
{
	char	*str;
	size_t	i;

	if ((str = (char *)malloc(sizeof(char) * (size + 1))) == NULL)
		return (NULL);
	i = -1;
	while (++i <= size)
		str[i] = 0;
	return (str);
}

int	ft_atoi(const char *string)
{
	int i;
	int number;
	int sign;

	i = 0;
	number = 0;
	sign = 1;
	while (string[i] == ' ' || string[i] == '\n' || string[i] == '\t'
			|| string[i] == '\f' || string[i] == '\r' || string[i] == '\v')
		i++;
	if (string[i] == '-' || string[i] == '+')
	{
		if (string[i] == '-')
			sign = -1;
		i++;
	}
	while (string[i] >= '0' && string[i] <= '9')
	{
		number *= 10;
		number += (int)string[i] - '0';
		i++;
	}
	return (number * sign);
}

char	*ft_itoa_u(unsigned long long int n)
{
	char					*str;
	unsigned long long int	i;
	int						n_len;

	n_len = (n < 0) ? 2 : 1;
	i = n;
	while (i /= 10)
		n_len++;
	if (!(str = (char *)malloc(sizeof(char) * n_len)))
		return (NULL);
	(n < 0) ? str[0] = '-' : 0;
	str[n_len] = '\0';
	i = n;
	(n < 0) ? i = -i : 0;
	while (n_len--)
	{
		str[n_len] = i % 10 + '0';
		if ((i /= 10) == 0)
			break ;
	}
	return (str);
}

char	*ft_itoa(intmax_t n)
{
	char			*str;
	long long int	i;
	int				n_len;

	n_len = (n < 0) ? 2 : 1;
	i = n;
	while (i /= 10)
		n_len++;
	if (!(str = (char *)malloc(sizeof(char) * n_len)))
		return (NULL);
	(n < 0) ? str[0] = '-' : 0;
	str[n_len] = '\0';
	i = n;
	(n < 0) ? i = -i : 0;
	while (n_len--)
	{
		str[n_len] = i % 10 + '0';
		if ((i /= 10) == 0)
			break ;
	}
	return (str);
}

char	*ftoa_d(long double num, int precision)
{
	int		i;
	int		j;
	char	*str;
	char	*res;

	i = -1;
	while (++i <= precision)
		num /= 0.1;
	str = ft_itoa((long long int)num + 1);
	res = (char *)malloc(sizeof(char) * ft_strlen(str));
	i = -1;
	j = 0;
	while ((unsigned long)++i < ft_strlen(str))
		if ((size_t)i == (ft_strlen(str) - precision - 1))
			res[i] = '.';
		else
		{
			res[i] = str[j];
			j++;
		}
	return (res);
}

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char	*str;
	size_t	i;

	if (s == '\0')
		return (NULL);
	if ((str = (char *)malloc(sizeof(char) * (len + 1))) == NULL)
		return (NULL);
	i = 0;
	while (s[start] != '\0' && i < len)
	{
		str[i] = s[start];
		i++;
		start++;
	}
	str[i] = '\0';
	return (str);
}

/*				-=FT_ITOA_BASE=-					*/

void f_big(long long int value, int base, char *str, int *i)
{
	char tmp[] = "0123456789ABCDEF";

	if (value <= -base || value >= base)
		f_big(value / base, base, str, i);
	str[(*i)++] = tmp[A(value % base)];
}

void f_small(long long int value, int base, char *str, int *i)
{
	char tmp[] = "0123456789abcdef";

	if (value <= -base || value >= base)
		f_small(value / base, base, str, i);
	str[(*i)++] = tmp[A(value % base)];
}

char *ft_itoa_base(long long int value, int base, int size)
{
	int i;
	char *str;

	i = 0;
	if (base < 2 || base > 16 || !(str = (char*)malloc(32)))
		return (0);
	if (base == 10 && (value < 0))
		str[i++]='-';
	if (size == 0)
		f_small(value, base, str, &i);
	else
		f_big(value, base, str, &i);
	str[i] = '\0';
	return (str);
}
