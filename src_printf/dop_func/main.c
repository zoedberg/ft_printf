/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gsominsk <gsominsk@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/31 19:26:10 by gsominsk          #+#    #+#             */
/*   Updated: 2017/02/03 15:29:20 by gsominsk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*нет юникода*/

// if (str[*start] == 's')
// 	func_string(descr);
// else if (str[*start] == 'p')
// 	func_pointer(descr);
// else if (str[*start] == 'd')
// 	func_int(descr, type);
// else if (str[*start] == 'D')
// 	func_long_decimal(descr, type);
// else if (str[*start] == 'i')
// 	func_int(descr, type);
// else if (str[*start] == 'f')
// 	func_float_small(descr);
// else if (str[*start] == 'F')
// 	func_float_big(descr, type);
// else if (str[*start] == 'u')
// 	func_unsigned(descr, type);
// else if (str[*start] == 'o')
// 	func_octa(descr, type);
// else if (str[*start] == 'c')
// 	func_char(descr);
// else if (str[*start] == 'b')
// 	func_binary(descr, type);
// else if (str[*start] == 'x')
// 	func_hex_small(descr, type);
// else if (str[*start] == 'X')
// 	func_hex_big(descr, type);

#include <stdio.h>
#include "../ft_printf.h"
#include <wchar.h>

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[91m"
#define KGRN  "\x1B[92m"
#define KYEL  "\x1B[93m"
#define KBLU  "\x1B[94m"
#define KMAG  "\x1B[95m"
#define KCYN  "\x1B[96m"
#define KWHT  "\x1B[97m"

int main(void)
{

	/*				CHARACTER					*/
	// char					_char;
	// signed char				_signed_char;
	// unsigned char			_unsigned_char;
	//
	// /*				DECIMAL					*/
	// short					_short;
	// short int				_short_int;
	// signed short			_signed_short;
	// signed short int		_signed_short_int;
	// unsigned short			_unsigned_short;
	// unsigned short int		_unsigned_short_int;
	// int						_int;
	// signed					_signed;
	// signed int				_signed_int;
	// unsigned				_unsigned;
	// unsigned int			_unsigned_int;
	// long					_long;
	// long int				_long_int;
	// signed long				_signed_long;
	// signed long int			_signed_long_int;
	// unsigned long			_unsigned_long;
	// unsigned long int		_unsigned_long_int;
	// long long				_long_long;
	// long long int			_long_long_int;
	// signed long long		_signed_long_long;
	// signed long long int	_signed_long_long_int;
	// unsigned long long		_unsigned_long_long;
	// unsigned long long int	_unsigned_long_long_int;
	// long long				_long_long;
	//
	// /*				FLOAT						*/
	// float	double			_float_double;
	// long double				_long_double;

	/*				OUR_PART					*/

	short					_short;
	long					_long;

	int						_int;
	short int				_short_int;
	unsigned short int		_unsigned_short_int;

	long int				_long_int;
	unsigned long int		_unsigned_long_int;

	unsigned long long int	_unsigned_long_long_int;
	long long int			_long_long_int;

	signed char				_signed_char;
	unsigned char			_unsigned_char;

	float					_float;
	double					_double;

	long double				_long_double;

	char					*p_1;
	int						*p_2;

	/*==========================================*/
	/*				TESTING_PART				*/
	/*==========================================*/

	/*==========================================*/
	_short = 0;
	_long = 0;
	_int = 10;
	_short_int = 0;
	_unsigned_short_int = 0;
	_long_int = 0;
	_unsigned_long_int = 0;
	_unsigned_long_long_int = 0;
	_long_long_int = 0;
	_signed_char = 65;
	_unsigned_char = 0;
	_float = 0;
	_double = 0;
	_long_double = 0;
	/*==========================================*/
	_short += 10;
	_long += 10;
	_int += 10;
	_short_int += 10;
	_unsigned_short_int += 10;
	_long_int += 10;
	_unsigned_long_int += 10;
	_unsigned_long_long_int += 10;
	_long_long_int += 10;
	_signed_char += 10;
	_unsigned_char += 10;
	_float += 10;
	_double += 10;
	_long_double += 10;
	/*==========================================*/
	p_1 = (char *)&_signed_char;
	p_2 = &_int;

	// for(int w=0; w < 0x110000; w++)
	// {
    // if(w<128) putchar(w);
    // else if(w<2048) { putchar(w>>6 | 0xC0); putchar(w&63 | 0x80); }
    // else if(w<1<<16) { putchar(w>>12 | 0xE0); putchar(w>>6 & 63 | 0x80); putchar(w&63 | 0x80);}
    // else if(w<1<<21) { putchar(w>>18 | 0xF0); putchar(w>>12 & 63 | 0x80); putchar(w>>6 & 63 | 0x80); putchar(w&63 | 0x80); }
    // else if(w<1<<26){ putchar(w>>24 | 0xF8); putchar(w>>18 & 63 | 0x80); putchar(w>>12 & 63 | 0x80); putchar(w>>6 & 63 | 0x80); putchar(w&63 | 0x80); }
    // else{ putchar(w>>30 | 0xFC); putchar(w>>24 & 63 | 0x80); putchar(w>>18 & 63 | 0x80); putchar(w>>12 & 63 | 0x80); putchar(w>>6 & 63 | 0x80); putchar(w&63 | 0x80); }
	// }
	//
	// ft_printf(KRED "==================================================%s\n", KNRM);
	// ft_printf(KRED "$$                 TRU_PRINTF                   $$%s\n", KNRM);
	// ft_printf(KRED "==================================================%s\n", KNRM);
	//
	// ft_printf(KRED "==================================================%s\n", KNRM);
	// ft_printf(KRED "$$                 -= ERRORS =-                 $$%s\n", KNRM);
	// ft_printf(KRED "==================================================%s\n", KNRM);
	//
	// ft_printf(KYEL "****************%s\n", KNRM);
	// ft_printf(KYEL "*  [no param]  *%s\n", KNRM);
	// ft_printf(KGRN "****************%s\n", KNRM);
	// ft_printf("\nstring       [s] : % s \n");
	// ft_printf("\ndecimal      [d] : % \n");
	// ft_printf("\ninteger      [i] : %  i \n",  5);
	// ft_printf(KGRN "\n****************%s\n", KNRM);
	//
	// ft_printf(KRED "==================================================%s\n", KNRM);
	// ft_printf(KRED "$$                 -= FLOAT =-                  $$%s\n", KNRM);
	// ft_printf(KRED "==================================================%s\n", KNRM);
	//
	// ft_printf(KYEL "****************%s\n", KNRM);
	// ft_printf(KYEL "*[  float %%f]  *%s\n", KNRM);
	// ft_printf(KGRN "****************%s\n", KNRM);
	// ft_printf("float       [f] : %.2f \n",  10.123);
	// ft_printf("double      [f] : %f \n", (double)10.123);
	// ft_printf("long double [f] : %Lf \n", (long double)10.123);
	// ft_printf(KGRN "****************%s\n", KNRM);
	//
	// ft_printf(KYEL "****************%s\n", KNRM);
	// ft_printf(KYEL "*[intmax_t %%D]*%s\n", KNRM);
	// ft_printf(KGRN "****************%s\n", KNRM);
	// ft_printf("double      [ld] : %D \n", 2147483647214748364821474836472147483648);
	// ft_printf(KGRN "****************%s\n", KNRM);
	//
	// ft_printf(KRED "==================================================%s\n", KNRM);
	// ft_printf(KRED "$$            -= SIZE_SPECIFIER =-              $$%s\n", KNRM);
	// ft_printf(KRED "==================================================%s\n", KNRM);
	//
	// ft_printf(KYEL "****************%s\n", KNRM);
	// ft_printf(KYEL "*[integer %%lli]*%s\n", KNRM);
	// ft_printf(KGRN "****************%s\n", KNRM);
	// ft_printf("first : %lli \n",  (long long int)21474327);
	// ft_printf("second : %li \n", (long)21474327);
	// ft_printf("second : %hhi \n", (signed char)65);
	// ft_printf("second : %hi \n", (short int)21474327);
	// ft_printf("second : %zi \n", (size_t)21474327);
	// ft_printf("second : %Li \n", (long long int)21474327);
	// ft_printf(KGRN "****************%s\n", KNRM);
	//
	// ft_printf(KYEL "****************%s\n", KNRM);
	// ft_printf(KYEL "*   [hex %%x]   *%s\n", KNRM);
	// ft_printf(KGRN "********************************%s\n", KNRM);
	// ft_printf("long long int [llx] : %llx \n",  (long long int)21474327);
	// ft_printf("long          [lx]  : %lx \n", (long)21474327);
	// ft_printf("signed char   [hhx] : %hhx \n", (signed char)65);
	// ft_printf("short int     [hx]  : %hx \n", (short int)21474327);
	// ft_printf("size_t        [zx]  : %zx \n", (size_t)21474327);
	// ft_printf("long long int [Lx]  : %Lx \n", (long long int)21474327);
	// ft_printf(KGRN "********************************%s\n", KNRM);
	//
	// ft_printf(KYEL "****************%s\n", KNRM);
	// ft_printf(KYEL "*   [octa %%o]  *%s\n", KNRM);
	// ft_printf(KGRN "********************************%s\n", KNRM);
	// ft_printf("long long int [llo] : %llo \n",  (long long int)21474327);
	// ft_printf("long          [lo]  : %lo \n", (long)21474327);
	// ft_printf("signed char   [hho] : %hho \n", (signed char)65);
	// ft_printf("short int     [ho]  : %ho \n", (short int)21474327);
	// ft_printf("size_t        [zo]  : %zo \n", (size_t)21474327);
	// ft_printf("long long int [Lo]  : %Lo \n", (long long int)21474327);
	// ft_printf(KGRN "********************************%s\n", KNRM);
	//
	// ft_printf(KYEL "****************%s\n", KNRM);
	// ft_printf(KYEL "* [unsigned %%u]*%s\n", KNRM);
	// ft_printf(KGRN "********************************%s\n", KNRM);
	// ft_printf("long long int [llu] : %llu \n",  (long long int)21474327);
	// ft_printf("long          [lu]  : %lu \n", (long)18446744073709551491);
	// ft_printf("signed char   [hhu] : %hhu \n", (signed char)65);
	// ft_printf("short int     [hu]  : %hu \n", (short int)21474327);
	// ft_printf("size_t        [zu]  : %zu \n", (size_t)21474327);
	// ft_printf("long long int [Lu]  : %Lu \n", (long long int)21474327);
	// ft_printf(KGRN "********************************%s\n", KNRM);
	//
	// ft_printf(KYEL "****************%s\n", KNRM);
	// ft_printf(KYEL "* [binary %%b]  *%s\n", KNRM);
	// ft_printf(KGRN "********************************%s\n", KNRM);
	// ft_printf("long long int [llb] : %llb \n",  (long long int)21474327);
	// ft_printf("long          [lb]  : %lb \n", (long)21474327);
	// ft_printf("signed char   [hhb] : %hhb \n", (signed char)65);
	// ft_printf("short int     [hb]  : %hb \n", (short int)21474327);
	// ft_printf("size_t        [zb]  : %zb \n", (size_t)21474327);
	// ft_printf("long long int [Lb]  : %Lb \n", (long long int)21474327);
	// ft_printf(KGRN "********************************%s\n", KNRM);
	//
	// ft_printf(KRED "==================================================%s\n", KNRM);
	// ft_printf(KRED "$$            -= TYPE_SPECIFIER =-              $$%s\n", KNRM);
	// ft_printf(KRED "==================================================%s\n", KNRM);
	//
	// ft_printf(KYEL "****************%s\n", KNRM);
	// ft_printf(KYEL "* [integer %%i] *%s\n", KNRM);
	// ft_printf(KGRN "****************%s\n", KNRM);
	// ft_printf("first : %d \n",  21474327);
	// ft_printf("second : % i \n", _int);
	// ft_printf(KGRN "****************%s\n", KNRM);
	//
	// ft_printf(KYEL "****************%s\n", KNRM);
	// ft_printf(KYEL "* [integer %%i] *%s\n", KNRM);
	// ft_printf(KGRN "****************%s\n", KNRM);
	// ft_printf("first  : %+03d \n",  42);
	// ft_printf("second : %+i \n", _int);
	// ft_printf(KGRN "****************%s\n", KNRM);
	//
	// ft_printf(KYEL "****************%s\n", KNRM);
	// ft_printf(KYEL "* [integer %%i] *%s\n", KNRM);
	// ft_printf(KGRN "****************%s\n", KNRM);
	// ft_printf("%% 4.5i  : |% 4.5i| \n",  42);
	// ft_printf("%%04.5i  : |%04.5i| \n",  42);
	// ft_printf("%%.5i    : |%0.5i| \n",  42);
	// ft_printf("%%+04.3i : |%+04.3i| \n",  42);
	// ft_printf("%%04.2i  : |%04.2i| \n",  42);
	// ft_printf("%%04.2i  : |%04.2i| \n",  42);
	// ft_printf("%%-04.2i : |%-04.3i| \n",  42);
	// ft_printf(KGRN "****************%s\n", KNRM);
	// //
	// ft_printf(KYEL "****************%s\n", KNRM);
	// ft_printf(KYEL "* [integer %%i] *%s\n", KNRM);
	// ft_printf(KGRN "****************%s\n", KNRM);
	// ft_printf("%% 4.5i  : |% 4.5i| \n",  4442);
	// ft_printf("%%+04.5i : |%+04.5i| \n",  4442);
	// ft_printf("%%04.5i  : |%04.5i| \n",  44442);
	// ft_printf("%%.5i    : |%0.5i| \n",  4442);
	// ft_printf("%%04.3i  : |%04.3i| \n",  4442);
	// ft_printf("%%04.2i  : |%04.2i| \n",  4442);
	// ft_printf("%%-04.2i : |%-04.2i| \n",  4442);
	// ft_printf(KGRN "****************%s\n", KNRM);
	//
	//
	// ft_printf(KYEL "****************%s\n", KNRM);
	// ft_printf(KYEL "*   [char %%i]  *%s\n", KNRM);
	// ft_printf(KGRN "****************%s\n", KNRM);
	// ft_printf("first : %c \n",  'a');
	// ft_printf("second : %c \n", 'b');
	// ft_printf(KGRN "****************%s\n", KNRM);
	//
	// ft_printf(KYEL "****************%s\n", KNRM);
	// ft_printf(KYEL "* [decimal %%d] *%s\n", KNRM);
	// ft_printf(KGRN "****************%s\n", KNRM);
	// ft_printf("first : %d \n",  _signed_char);
	// ft_printf("second : %d \n", _int);
	// ft_printf(KGRN "****************%s\n", KNRM);
	//
	// ft_printf(KYEL "****************%s\n", KNRM);
	// ft_printf(KYEL "* [string  %%s] *%s\n", KNRM);
	// ft_printf(KGRN "****************%s\n", KNRM);
	// ft_printf("first : %s \n",  "hello");
	// ft_printf("second : %s \n", "yep");
	// ft_printf(KYEL "****************%s\n", KNRM);
	//
	// ft_printf(KYEL "****************%s\n", KNRM);
	// ft_printf(KYEL "* [octa  %%o] *%s\n", KNRM);
	// ft_printf(KGRN "****************%s\n", KNRM);
	// ft_printf("first : %#o \n",  0);
	// ft_printf("second : %o \n", _int);
	// ft_printf(KGRN "****************%s\n", KNRM);
	//
	// ft_printf(KYEL "****************%s\n", KNRM);
	// ft_printf(KYEL "* [heximal %%x] *%s\n", KNRM);
	// ft_printf(KGRN "****************%s\n", KNRM);
	// ft_printf("first : %x \n",  _signed_char);
	// ft_printf("second : %x \n", _int);
	// ft_printf(KGRN "****************%s\n", KNRM);
	//
	//
	// ft_printf(KYEL "****************%s\n", KNRM);
	// ft_printf(KYEL "* [octa  %%o] *%s\n", KNRM);
	// ft_printf(KGRN "****************%s\n", KNRM);
	// ft_printf("first : %#o \n",  _signed_char);
	// ft_printf("second : %o \n", _int);
	// ft_printf(KGRN "****************%s\n", KNRM);
	//
	// ft_printf(KYEL "****************%s\n", KNRM);
	// ft_printf(KYEL "* [heximal %%x] *%s\n", KNRM);
	// ft_printf(KGRN "****************%s\n", KNRM);
	// ft_printf("first : %#x \n",  _signed_char);
	// ft_printf("second : %x \n", _int);
	// ft_printf(KGRN "****************%s\n", KNRM);
	//
	// ft_printf(KYEL "****************%s\n", KNRM);
	// ft_printf(KYEL "* [heximal %%x] *%s\n", KNRM);
	// ft_printf(KGRN "****************%s\n", KNRM);
	// ft_printf("first : %#X \n",  _signed_char);
	// ft_printf("second : %X \n", _int);
	// ft_printf(KGRN "****************%s\n", KNRM);
	//
	// ft_printf(KYEL "****************%s\n", KNRM);
	// ft_printf(KYEL "* [heximal %%X] *%s\n", KNRM);
	// ft_printf(KGRN "****************%s\n", KNRM);
	// ft_printf("first : %X \n",  _signed_char);
	// ft_printf("second : %X \n", _int);
	// ft_printf(KGRN "****************%s\n", KNRM);
	//
	// ft_printf(KYEL "****************%s\n", KNRM);
	// ft_printf(KYEL "* [pointer %%p] *%s\n", KNRM);
	// ft_printf(KGRN "****************%s\n", KNRM);
	// ft_printf("p_1  : %p \n",  p_1);
	// ft_printf("&p_1 : %p \n", &p_1);
	// ft_printf(KGRN "****************%s\n", KNRM);
	// //
	// ft_printf(KYEL "****************%s\n", KNRM);
	// ft_printf(KYEL "* [pointer %%p] *%s\n", KNRM);
	// ft_printf(KGRN "****************%s\n", KNRM);
	// ft_printf("p_2  : %p \n",  p_2);
	// ft_printf("&_p2 : %p \n", &p_2);
	// ft_printf(KGRN "****************%s\n", KNRM);
	//
	// printf(KRED "==================================================%s\n", KNRM);
	// printf(KRED "$$                 NE_TRU_PRINTF                   $$%s\n", KNRM);
	// printf(KRED "==================================================%s\n", KNRM);
	//
	// printf(KRED "==================================================%s\n", KNRM);
	// printf(KRED "$$                 -= ERRORS =-                 $$%s\n", KNRM);
	// printf(KRED "==================================================%s\n", KNRM);
	//
	// printf(KYEL "****************%s\n", KNRM);
	// printf(KYEL "*  [no param]  *%s\n", KNRM);
	// printf(KGRN "****************%s\n", KNRM);
	// printf("\nstring       [s] : % s \n");
	// printf("\ndecimal      [d] : % \n");
	// printf("\ninteger      [i] : %  i \n",  5);
	// printf(KGRN "\n****************%s\n", KNRM);
	//
	// printf(KRED "==================================================%s\n", KNRM);
	// printf(KRED "$$                 -= FLOAT =-                  $$%s\n", KNRM);
	// printf(KRED "==================================================%s\n", KNRM);
	//
	// printf(KYEL "****************%s\n", KNRM);
	// printf(KYEL "*[  float %%f]  *%s\n", KNRM);
	// printf(KGRN "****************%s\n", KNRM);
	// printf("float       [f] : %.2f \n",  10.123);
	// printf("double      [f] : %f \n", (double)10.123);
	// printf("long double [f] : %Lf \n", (long double)10.123);
	// printf(KGRN "****************%s\n", KNRM);
	//
	// printf(KYEL "****************%s\n", KNRM);
	// printf(KYEL "*[intmax_t %%D]*%s\n", KNRM);
	// printf(KGRN "****************%s\n", KNRM);
	// printf("double      [ld] : %D \n", 2147483647214748364821474836472147483648);
	// printf(KGRN "****************%s\n", KNRM);
	//
	// printf(KRED "==================================================%s\n", KNRM);
	// printf(KRED "$$            -= SIZE_SPECIFIER =-              $$%s\n", KNRM);
	// printf(KRED "==================================================%s\n", KNRM);
	//
	// printf(KYEL "****************%s\n", KNRM);
	// printf(KYEL "*[integer %%lli]*%s\n", KNRM);
	// printf(KGRN "****************%s\n", KNRM);
	// printf("first : %lli \n",  (long long int)21474327);
	// printf("second : %li \n", (long)21474327);
	// printf("second : %hhi \n", (signed char)65);
	// printf("second : %hi \n", (short int)21474327);
	// printf("second : %zi \n", (size_t)21474327);
	// printf("second : %Li \n", (long long int)21474327);
	// printf(KGRN "****************%s\n", KNRM);
	//
	// printf(KYEL "****************%s\n", KNRM);
	// printf(KYEL "*   [hex %%x]   *%s\n", KNRM);
	// printf(KGRN "********************************%s\n", KNRM);
	// printf("long long int [llx] : %llx \n",  (long long int)21474327);
	// printf("long          [lx]  : %lx \n", (long)21474327);
	// printf("signed char   [hhx] : %hhx \n", (signed char)65);
	// printf("short int     [hx]  : %hx \n", (short int)21474327);
	// printf("size_t        [zx]  : %zx \n", (size_t)21474327);
	// printf("long long int [Lx]  : %Lx \n", (long long int)21474327);
	// printf(KGRN "********************************%s\n", KNRM);
	//
	// printf(KYEL "****************%s\n", KNRM);
	// printf(KYEL "*   [octa %%o]  *%s\n", KNRM);
	// printf(KGRN "********************************%s\n", KNRM);
	// printf("long long int [llo] : %llo \n",  (long long int)21474327);
	// printf("long          [lo]  : %lo \n", (long)21474327);
	// printf("signed char   [hho] : %hho \n", (signed char)65);
	// printf("short int     [ho]  : %ho \n", (short int)21474327);
	// printf("size_t        [zo]  : %zo \n", (size_t)21474327);
	// printf("long long int [Lo]  : %Lo \n", (long long int)21474327);
	// printf(KGRN "********************************%s\n", KNRM);
	//
	// printf(KYEL "****************%s\n", KNRM);
	// printf(KYEL "* [unsigned %%u]*%s\n", KNRM);
	// printf(KGRN "********************************%s\n", KNRM);
	// printf("long long int [llu] : %llu \n",  (long long int)21474327);
	// printf("long          [lu]  : %lu \n", (long)18446744073709551491);
	// printf("signed char   [hhu] : %hhu \n", (signed char)65);
	// printf("short int     [hu]  : %hu \n", (short int)21474327);
	// printf("size_t        [zu]  : %zu \n", (size_t)21474327);
	// printf("long long int [Lu]  : %Lu \n", (long long int)21474327);
	// printf(KGRN "********************************%s\n", KNRM);
	//
	// printf(KYEL "****************%s\n", KNRM);
	// printf(KYEL "* [binary %%b]  *%s\n", KNRM);
	// printf(KGRN "********************************%s\n", KNRM);
	// printf("long long int [llb] : %llb \n",  (long long int)21474327);
	// printf("long          [lb]  : %lb \n", (long)21474327);
	// printf("signed char   [hhb] : %hhb \n", (signed char)65);
	// printf("short int     [hb]  : %hb \n", (short int)21474327);
	// printf("size_t        [zb]  : %zb \n", (size_t)21474327);
	// printf("long long int [Lb]  : %Lb \n", (long long int)21474327);
	// printf(KGRN "********************************%s\n", KNRM);
	//
	// printf(KRED "==================================================%s\n", KNRM);
	// printf(KRED "$$            -= TYPE_SPECIFIER =-              $$%s\n", KNRM);
	// printf(KRED "==================================================%s\n", KNRM);
	//
	// printf(KYEL "****************%s\n", KNRM);
	// printf(KYEL "* [integer %%i] *%s\n", KNRM);
	// printf(KGRN "****************%s\n", KNRM);
	// printf("first : %d \n",  21474327);
	// printf("second : % i \n", _int);
	// printf(KGRN "****************%s\n", KNRM);
	//
	// printf(KYEL "****************%s\n", KNRM);
	// printf(KYEL "* [integer %%i] *%s\n", KNRM);
	// printf(KGRN "****************%s\n", KNRM);
	// printf("first  : %+03d \n",  42);
	// printf("second : %+i \n", _int);
	// printf(KGRN "****************%s\n", KNRM);
	// //
	// printf(KYEL "****************%s\n", KNRM);
	// printf(KYEL "* [integer %%i] *%s\n", KNRM);
	// printf(KGRN "****************%s\n", KNRM);
	// printf("%% 4.5i  : |% 4.5i| \n",  42);
	// printf("%%04.5i  : |%04.5i| \n",  42);
	// printf("%%.5i    : |%0.5i| \n",  42);
	// printf("%%+04.3i : |%+04.3i| \n",  42);
	// printf("%%04.2i  : |%04.2i| \n",  42);
	// printf("%%04.2i  : |%04.2i| \n",  42);
	// printf("%%-04.2i : |%-04.3i| \n",  42);
	// printf(KGRN "****************%s\n", KNRM);
	// //
	// printf(KYEL "****************%s\n", KNRM);
	// printf(KYEL "* [integer %%i] *%s\n", KNRM);
	// printf(KGRN "****************%s\n", KNRM);
	// printf("%% 4.5i  : |% 4.5i| \n",  4442);
	// printf("%%+04.5i : |%+04.5i| \n",  4442);
	// printf("%%04.5i  : |%04.5i| \n",  44442);
	// printf("%%.5i    : |%0.5i| \n",  4442);
	// printf("%%04.3i  : |%04.3i| \n",  4442);
	// printf("%%04.2i  : |%04.2i| \n",  4442);
	// printf("%%-04.2i : |%-04.2i| \n",  4442);
	// printf(KGRN "****************%s\n", KNRM);
	//
	// printf(KYEL "****************%s\n", KNRM);
	// printf(KYEL "*   [char %%i]  *%s\n", KNRM);
	// printf(KGRN "****************%s\n", KNRM);
	// printf("first : %c \n",  'a');
	// printf("second : %c \n", 'b');
	// printf(KGRN "****************%s\n", KNRM);
	//
	// printf(KYEL "****************%s\n", KNRM);
	// printf(KYEL "* [decimal %%d] *%s\n", KNRM);
	// printf(KGRN "****************%s\n", KNRM);
	// printf("first : %d \n",  _signed_char);
	// printf("second : %d \n", _int);
	// printf(KGRN "****************%s\n", KNRM);
	//
	// printf(KYEL "****************%s\n", KNRM);
	// printf(KYEL "* [string  %%s] *%s\n", KNRM);
	// printf(KGRN "****************%s\n", KNRM);
	// printf("first : %s \n",  "hello");
	// printf("second : %s \n", "yep");
	// printf(KYEL "****************%s\n", KNRM);
	//
	// printf(KYEL "****************%s\n", KNRM);
	// printf(KYEL "* [octa  %%o] *%s\n", KNRM);
	// printf(KGRN "****************%s\n", KNRM);
	// printf("first : %#o \n",  0);
	// printf("second : %o \n", _int);
	// printf(KGRN "****************%s\n", KNRM);
	//
	// printf(KYEL "****************%s\n", KNRM);
	// printf(KYEL "* [heximal %%x] *%s\n", KNRM);
	// printf(KGRN "****************%s\n", KNRM);
	// printf("first : %x \n",  _signed_char);
	// printf("second : %x \n", _int);
	// printf(KGRN "****************%s\n", KNRM);
	//
	//
	// printf(KYEL "****************%s\n", KNRM);
	// printf(KYEL "* [octa  %%o] *%s\n", KNRM);
	// printf(KGRN "****************%s\n", KNRM);
	// printf("first : %#o \n",  _signed_char);
	// printf("second : %o \n", _int);
	// printf(KGRN "****************%s\n", KNRM);
	//
	// printf(KYEL "****************%s\n", KNRM);
	// printf(KYEL "* [heximal %%x] *%s\n", KNRM);
	// printf(KGRN "****************%s\n", KNRM);
	// printf("first : %#x \n",  _signed_char);
	// printf("second : %x \n", _int);
	// printf(KGRN "****************%s\n", KNRM);
	//
	// printf(KYEL "****************%s\n", KNRM);
	// printf(KYEL "* [heximal %%x] *%s\n", KNRM);
	// printf(KGRN "****************%s\n", KNRM);
	// printf("first : %#X \n",  _signed_char);
	// printf("second : %X \n", _int);
	// printf(KGRN "****************%s\n", KNRM);
	//
	// printf(KYEL "****************%s\n", KNRM);
	// printf(KYEL "* [heximal %%X] *%s\n", KNRM);
	// printf(KGRN "****************%s\n", KNRM);
	// printf("first : %X \n",  _signed_char);
	// printf("second : %X \n", _int);
	// printf(KGRN "****************%s\n", KNRM);
	//
	// printf(KYEL "****************%s\n", KNRM);
	// printf(KYEL "* [pointer %%p] *%s\n", KNRM);
	// printf(KGRN "****************%s\n", KNRM);
	// printf("p_1  : %p \n",  p_1);
	// printf("&p_1 : %p \n", &p_1);
	// printf(KGRN "****************%s\n", KNRM);
	// //
	// printf(KYEL "****************%s\n", KNRM);
	// printf(KYEL "* [pointer %%p] *%s\n", KNRM);
	// printf(KGRN "****************%s\n", KNRM);
	// printf("p_2  : %p \n",  p_2);
	// printf("&_p2 : %p \n", &p_2);
	// printf(KGRN "****************%s\n", KNRM);

	return 0;
}
